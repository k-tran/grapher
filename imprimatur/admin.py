from django.contrib import admin
from imprimatur.models import Unigram, Bigram, Trigram, Fourgram, Fivegram, Sixgram, Year
# Register your models here.

admin.site.register(Year)
admin.site.register(Unigram)
admin.site.register(Bigram)
admin.site.register(Trigram)
admin.site.register(Fourgram)
admin.site.register(Fivegram)
admin.site.register(Sixgram)
