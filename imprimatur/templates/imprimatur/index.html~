{% extends 'base.html' %}
{% load static %}
{% block title %}Graph{% endblock %}

{% block head_block %}
    <style>
	body {
  		background: url("{% static 'img/background.jpg' %}") no-repeat center center fixed; 
		-webkit-background-size: cover;
		-moz-background-size: cover;
		-o-background-size: cover;
		background-size: cover;
		color: white;
		}
	
	svg { font: 10px sans-serif; }

	
	.line {	stroke: steelblue;
  		stroke-width: 3;
  		fill: none;
		}

	.axis text { 	font: 10px sans-serif;
			fill: white;
       		}

	.axis path,
	.axis line { fill: none;
		     stroke: grey;
	  	     stroke-width: 1;
		     shape-rendering: crispEdges;
			}

	.overlay { fill: none;
	  	   pointer-events: all;
		}

	.focus circle { fill: none;
	  		stroke: steelblue;
			}
	
	.focus text { fill: white; }

	<! div.tooltip {	
	    position: absolute;			
	    text-align: left;			
	    width: 60px;					
	    height: 28px;					
	    padding: 2px;				
	    font: 10px sans-serif;		
	    background: lightsteelblue;	
	    border: 0px;		
	    border-radius: 8px;			
	    pointer-events: none;			
	}>

    </style>

    <script src="{% static "js/d3.v3.min.js" %}"></script>	

{% endblock %}

{% block body_block %}
      <div class="container">
	
	{% if error %}
      <div class="jumbotron">
        <h1>Phrase Grapher</h1>
	<div class="container"><p>Your search term "{{ query }}" is longer than one word.  Phrase searching will be available soon.  In the meantime, please search for single words.</p></div>
	<div class="container">
		<form class="navbar-form pull-right col-sm-4" id="user_form" method="post" action="{% url 'imprimatur:imp_results' %}">
{% csrf_token %}
		<input type="text" name="query" value="" id="query" class="form-control" placeholder="Enter single word"/>
		<input type="submit" name="submit" class="btn btn-success" value="Search again" />
		</form>
	</div>
      </div>


        {% elif query %}
		<h1>Phrase Grapher</h1>
		<div class="container">
		<p>Phrase searched: {{ query }}</p>
		</div>
		<div class="container">
			<form class="navbar-form pull-right col-sm-4" id="user_form" method="post" action="{% url 'imprimatur:imp_results' %}">
	{% csrf_token %}
			<input type="text" name="query" value="" id="query" class="form-control" placeholder="Enter single word"/>
			<input type="submit" name="submit" class="btn btn-success" value="Search again" />
			</form>

		</div>
		<svg class="chart"></svg>
	{% else %}
    
     

      <div class="jumbotron">
        <h1>Phrase Grapher</h1>
	<div class="container"><p>Plot the number of times a word has been used in judgments of the High Court
	</p></div>
	<div class="container">
		<form class="navbar-form pull-right col-sm-4" id="user_form" method="post" action="{% url 'imprimatur:imp_results' %}">
{% csrf_token %}
		<input type="text" name="query" value="" id="query" class="form-control" placeholder="Enter word"/>
		<input type="submit" name="submit" class="btn btn-success" value="Search" />
		</form>
	</div>
      </div>

      <div class="row marketing">
	  <div class="col-md-3"></div>
	  <div class="col-md-6">
        
  
	<h4>How to search</h4>
          <p>
	  <ul>
	    <li>Searches are case-insensitive (eg, "mabo" will yield "Mabo")</li>
	    <li>At present, searches are limited to one-word only</li>
	    <li>A version normalising the word-counts will soon be available</li>
	  </ul>
	  
	  </p>
	  </div>
	  <div class="col-md-3"></div>


      </div>

    </div>
	{% endif %}

<script>

var margin = {top: 20, right: 30, bottom: 50, left: 40},
	width = 960 - margin.left - margin.right,
	height = 500 - margin.top - margin.bottom;

var data = [
	{% for datum in data %}
	{year: {{ datum.year }}, value: {{ datum.count }}},
	{% endfor %}
    ];

var parseYear = d3.time.format("%Y").parse,
    formatYear = d3.time.format("%Y"),
    bisectYear = d3.bisector(function(d) { return d.year; }).left;

data.forEach(function(d) {
    d.year = parseYear(String(d.year));
    d.value = +d.value;
  });

var x = d3.time.scale()
	.range([0, width])
	.domain(d3.extent(data, function(d) { return d.year; }));

var y = d3.scale.linear()
	.domain([0, d3.max(data, function(d) { return d.value; })])
	.range([height, 0]);
	
var xAxis = d3.svg.axis()
	.scale(x)
	.orient("bottom");

var yAxis = d3.svg.axis()
	.scale(y)
	.orient("left");

var line = d3.svg.line()
	.x(function(d) { return x(d.year); })
	.y(function(d) { return y(d.value); });

// var div = d3.select("body").append("div")	
//    .attr("class", "tooltip")				
//    .style("opacity", 0);

var chart = d3.select(".chart")
	.attr("width", width + margin.left + margin.right)
	.attr("height", height + margin.top + margin.bottom)
  .append("g")
  	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// line
chart.append("path")
	.datum(data)
	.attr("class", "line")
	.attr("d", line);

// x-axis
chart.append("g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + height + ")")
	.call(xAxis);

// y-axis
chart.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("Mentions");

// focus
var focus = chart.append("g")
      .attr("class", "focus")
      .style("display", "none");

focus.append("circle")
      .attr("r", 4.5);

focus.append("text")
      .attr("x", 9)
      .attr("dy", ".35em");

chart.append("rect")
      .attr("class", "overlay")
      .attr("width", width)
      .attr("height", height)
      .on("mouseover", function() { focus.style("display", null); })
      .on("mouseout", function() { focus.style("display", "none"); })
      .on("mousemove", mousemove);

function mousemove() {
    var x0 = x.invert(d3.mouse(this)[0]),
        i = bisectYear(data, x0, 1),
        d0 = data[i - 1],
        d1 = data[i],
        d = x0 - d0.year > d1.year - x0 ? d1 : d0;
    focus.attr("transform", "translate(" + x(d.year) + ", " + y(d.value) + ")");
    focus.select("text").text(formatYear(d.year) + ", " + d.value);
}


</script>
<script>
	{% if user.is_authenticated %}
	document.getElementById("rankings").className = "inactive";
	{% endif %}
	document.getElementById("graph").className = "active";
	document.getElementById("register").className = "inactive";
	document.getElementById("login").className = "inactive";
</script>

{% endblock %}


