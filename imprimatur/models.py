from django.db import models

class Year(models.Model):
    year = models.IntegerField()

    def __unicode__(self):
        return str(self.year)

class Unigram(models.Model):
    phrase = models.CharField(max_length=128)
    count = models.IntegerField(default=0)
    year = models.ForeignKey(Year)

    def __unicode__(self):
        return u'%s, %s' % (self.phrase, self.year)

class Bigram(models.Model):
    phrase = models.CharField(max_length=128)
    count = models.IntegerField(default=0)
    year = models.ForeignKey(Year)

    def __unicode__(self):
        return u'%s, %s' % (self.phrase, self.year)

class Trigram(models.Model):
    phrase = models.CharField(max_length=128)
    count = models.IntegerField(default=0)
    year = models.ForeignKey(Year)

    def __unicode__(self):
        return u'%s, %s' % (self.phrase, self.year)

class Fourgram(models.Model):
    phrase = models.CharField(max_length=128)
    count = models.IntegerField(default=0)
    year = models.ForeignKey(Year)

    def __unicode__(self):
        return u'%s, %s' % (self.phrase, self.year)

class Fivegram(models.Model):
    phrase = models.CharField(max_length=128)
    count = models.IntegerField(default=0)
    year = models.ForeignKey(Year)

    def __unicode__(self):
        return u'%s, %s' % (self.phrase, self.year)

class Sixgram(models.Model):
    phrase = models.CharField(max_length=128)
    count = models.IntegerField(default=0)
    year = models.ForeignKey(Year)

    def __unicode__(self):
        return u'%s, %s' % (self.phrase, self.year)


