from django.shortcuts import render, render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.db.models import Q
import re, json
from django.core.cache import caches
from django.core.cache.backends.base import InvalidCacheBackendError
from nltk.util import ngrams
from imprimatur.models import Unigram, Sixgram
from nltk.tokenize import word_tokenize

def imprimatur_search(request):
    context = RequestContext(request)
    context_dict = {}
    
    return render_to_response('imprimatur/index.html', context_dict, context)

def imprimatur_results(request):
    context = RequestContext(request)
    if request.method == 'POST':

        query = request.POST['query'].strip()
        
        length = len(word_tokenize(query))
        phrases = []
        if length > 1:
            error = True
            context_dict = {
            'error': error,
            'query': query,
            }
            
        else:

            phrases = Unigram.objects.filter(phrase=query.lower()).order_by('year')
            year_list = []
            data = []
            for phrase in phrases:
                y = {'year': phrase.year.year, 'count': phrase.count}
                data.append(y)
            # manualy adding zero counts and years
            for phrase in phrases:
                year_list.append(phrase.year.year)
            
            [int(year) for year in year_list]

            for year in range(1903, 2015):
                if year not in year_list:
                    data.append({'year': year, 'count': 0})
                    print 'Appended', year
            
            data = sorted(data, key=lambda k: k['year'])

            context_dict = {
            'data': data,
            'phrases': phrases,
            'query': query,
            }

#        elif length == 2:
#            phrases = Bigram.objects.filter(phrase=query.lower()).order_by('year')
#        elif length == 3:
#            phrases = Trigram.objects.filter(phrase=query.lower()).order_by('year')
#        elif length == 4:
#            phrases = Fourgram.objects.filter(phrase=query.lower()).order_by('year')
#        elif length == 5:
#            phrases = Fivegram.objects.filter(phrase=query.lower()).order_by('year')
#        elif length == 6:
#            phrases = Sixgram.objects.filter(phrase=query.lower()).order_by('year')
    
    
    return render_to_response('imprimatur/index.html', context_dict, context)
