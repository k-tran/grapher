from django.conf.urls import patterns, include, url
from imprimatur import views

urlpatterns = patterns('',
        url(r'^$', views.imprimatur_search, name='imp_search'),
        url(r'^results/$', views.imprimatur_results, name='imp_results'),
)
