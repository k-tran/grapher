from bs4 import BeautifulSoup
import re, csv, urllib2

def parse_judgment(court_path, count):
	
	#uncomment the following to make soup from URL and not local file

	#url = "http://www.austlii.edu.au/au/cases/nsw/NSWSC/2014/{0}.html".format(judgment_count)
	#req = urllib2.Request(url, headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"})
	#page = urllib2.urlopen(req)
	#soup = BeautifulSoup(page.read(), 'lxml', from_encoding='Latin-1')

	url = '/Users/kelvintran/Dropbox/Python/PowerIndex/{0}/{1}.html'.format(court_path, count)
	soup = BeautifulSoup(open(url), 'lxml', from_encoding='Latin-1')
	
	court = soup.find("h1")
	
	case_name = soup.find(text=re.compile('^Case T')).find_next('div')
	case_name = tags.sub('', str(case_name))
	
	MNC = soup.find(text=re.compile('^Medium N')).find_next('div').text
	MNC = l_r_strip(MNC)
	
	file_num = soup.find(text=re.compile(r'File\s[Nn]')).find_next('div')
	file_num = tags.sub('', str(file_num))
	
	judge = soup.find(text=re.compile('^Before')).find_next('div')
	judge = tags.sub('', str(judge))
	
	date = soup.find(text=re.compile('^Decision Date:')).find_next('div')
	date = tags.sub('', str(date))
	
	decision = soup.find(text=re.compile('Decision:')).find_next('div')
	decision = tags.sub('', str(decision))
	
	if soup.find(text=re.compile('^Catchwords:')) == None:
		area_of_law = 'No catchwords'
	else:
		area_of_law = soup.find(text=re.compile('^Catchwords:')).find_next('div')
		area_of_law = tags.sub('', str(area_of_law))
		
	print "Case name:", case_name
	print "Citation:", MNC
	print judge
	print date
	print 'Area of law:', area_of_law
	print 'Decision:', decision

	[i.extract() for i in soup.findAll('br')]
	counsel_cat_long = soup.find(text=re.compile('-\sCounsel:'))
	if counsel_cat_long > -1:
		counsel_cat_long = counsel_cat_long.find_next('div')
		counsel_cat_list = re.findall(r'(?<=\()[0-9A-Za-z\s]+(?=\))', str(counsel_cat_long))
		counsel_cat_list = [tags.sub('', str(i)) for i in counsel_cat_list]
		
		counsel_split = re.split(r'(?<=\))\s', str(counsel_cat_long))
		counsel_split = [re.compile(r'Counsel:\s|\([0-9A-Za-z\s]+\)|\\n|<[^<]+?>').sub('', i) for i in counsel_split]
		counsel_split = [i.replace('\n', ' ') for i in counsel_split]
		counsel_split = [re.split(r'\/|\b\sand\s\b|\b\swith\s\b|,\s', i) for i in counsel_split]
		counsel_split = [[l_r_strip(n) for n in i] for i in counsel_split]
	else:
		counsel_cat_list = ['No counsel']
		counsel_split = counsel_cat_list
	
	print counsel_cat_list
	print counsel_split

	firms_role_long = soup.find(text=re.compile('-\sSolicitors:'))
	if firms_role_long > -1:
		firms_role_long = firms_role_long.find_next('div')
		firms_role = re.findall(r'(?<=\()[0-9A-Za-z\s]+(?=\))', str(firms_role_long)) #find between ()
		firms_role = [tags.sub('', str(i)) for i in firms_role] #remove tags
		
		firms = re.split(r'(?<=\))\s', str(firms_role_long)) #split at space after ')'
		firms = [re.compile('Solicitors:\s|\([0-9A-Za-z\s]+\)|\\n|<[^<]+?>').sub('', i) for i in firms]
		firms = [l_r_strip(str(i)) for i in firms]
	else:
		firms_role = ['No instructing solicitor category']
		firms = ['No instructing solicitors']

	print firms_role
	print firms

	times_to_append = abs(len(firms_role) - len(counsel_cat_list))

	if len(firms_role) > len(counsel_cat_list):	
		[counsel_cat_list.append('No counsel category') for i in range(times_to_append)]
		[counsel_split.append(['No counsel appearing']) for i in range(times_to_append)]
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firms_role):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firms_role[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1

	elif len(firms_role) < len(counsel_cat_list):
		[firms_role.append('No instructing solicitor category') for i in range(times_to_append)]
		[firms.append('No instructing solicitor') for i in range(times_to_append)]
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firms_role):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firms_role[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1
	else:
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firms_role):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firms_role[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1

tags = re.compile('<[^<]+?>')
date_appearance_comp = re.compile('\sappeared\son\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+|\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+')
def l_r_strip(str):
	str = str.lstrip()
	str = str.rstrip()
	return str
solicitor_at_firm = re.compile('.+(of)\s|.+,\s|.+(from)\s')

judgment_count = 196
#parse_judgment('NSWSC', judgment_count)

ignore_list = [3, 44, 58, 68]
while judgment_count < 1500:
	if judgment_count in ignore_list:
		judgment_count += 1
	else:
		try:
			parse_judgment('NSWSC', judgment_count)
			judgment_count += 1
		except IOError:
			judgment_count += 1

#44 - selp repd