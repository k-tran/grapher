from bs4 import BeautifulSoup
import re, csv, urllib2

def parse_judgment(court_path, count):
	
	#uncomment the following to make soup from URL and not local file

	#url = "http://www.austlii.edu.au/au/cases/cth/FCAFC/2014/{0}.html".format(judgment_count)
	#req = urllib2.Request(url, headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"})
	#page = urllib2.urlopen(req)
	#soup = BeautifulSoup(page.read(), 'lxml', from_encoding='Latin-1')


	url = '/Users/kelvintran/Dropbox/Python/PowerIndex/{0}/{1}.html'.format(court_path, count)
	soup = BeautifulSoup(open(url), 'lxml', from_encoding='Latin-1')
	
	court = soup.find("h1")	
	
	citation = soup.find(text=re.compile('Citation')).findNext('td').div
	citation = tags.sub('', str(citation))
	citation = citation.replace('\n', ' ')
	citation = re.split(r'\s\s?(?=\[)', citation)
	
	case_name = citation[0]
	MNC = citation[1]
	
	br_tags = soup.find_all('br')
	
	file_num = soup.find(text=re.compile('^File number'))
	if file_num > -1:
		file_num = file_num.findNext('td').div
		file_num = file_num.renderContents()
	
	judge = soup.find(text=re.compile('^Judge')).findNext('div')
	judge = tags.sub('', str(judge))
	
	date = soup.find(text=re.compile('^Date of')).findNext('div')
	date = str(date)
	date = tags.sub('', date)
	
	if soup.find(text=re.compile('^Catchwords:')) == None:
		area_of_law = 'No catchwords'
	else:
		area_of_law_with_tag = soup.find(text=re.compile('^Catchwords:')).findNext('b')
		area_of_law_with_tag = str(area_of_law_with_tag)
		area_of_law = tags.sub('', area_of_law_with_tag)
	
	counsel_cat_list_with_newline = soup.findAll(text=re.compile('(^Counsel for\s[A-Za-z]+\s[A-Z][^:]+)|^Counsel for the proposed\s\d+|^Counsel for [A-Za-z\d]+\s[A-Za-z\d]+(\s[A-Za-z\d]+)?(\s[A-Za-z\d]+)?(?=:)'))
	counsel_cat_list = [i.replace('\n', ' ') for i in counsel_cat_list_with_newline]
	
	counsel_split = [i.findNext('td') for i in counsel_cat_list_with_newline]
	counsel_split = [tags.sub('', str(i)) for i in counsel_split]
	counsel_split = [i.replace('\n', ' ') for i in counsel_split]
	counsel_split = [date_appearance_comp.sub('', str(i)) for i in counsel_split]
	counsel_split = [whitespace_before_after.sub('', str(i)) for i in counsel_split]
	counsel_split = [re.split(r'\b\sand\s\b|\b\swith\s\b|,\s', i) for i in counsel_split]
	
	firm_cat = soup.findAll(text=re.compile('^Solicitor for'))
	
	firms = [i.findNext('div').renderContents() for i in firm_cat]
	firms = [tags.sub('', i) for i in firms]
	firms = [solicitor_at_firm.sub('', i) for i in firms]
		
	print "Citation:", citation
	print "Court:", court.renderContents()

	print "File number:", file_num
	print judge
	print date
	print "counsel_cat_list", counsel_cat_list
	print "counsel_split", counsel_split
	print "firm_cat", firm_cat
	print "firms", firms
	print "Area of law:", area_of_law

	times_to_append = abs(len(firm_cat) - len(counsel_cat_list))
	with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			opened_csv.writerow(['Case name', 'Citation', 'Court', 'File number', 'Judge', 'Date', 'Counsel Role', 'Counsel', 'Firm role', 'Firm'])
	if len(firm_cat) > len(counsel_cat_list):	
		[counsel_cat_list.append('No counsel category') for i in range(times_to_append)]
		[counsel_split.append(['No counsel appearing']) for i in range(times_to_append)]
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firm_cat):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firm_cat[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1

	elif len(firm_cat) < len(counsel_cat_list):
		[firm_cat.append('No instructing solicitor category') for i in range(times_to_append)]
		[firms.append('No instructing solicitor') for i in range(times_to_append)]
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firm_cat):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firm_cat[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1
	else:
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firm_cat):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firm_cat[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1

tags = re.compile('<[^<]+?>')
date_appearance_comp = re.compile('\sappeared\son\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+|\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+')
whitespace_before_after = re.compile('^\s|\s$')
solicitor_at_firm = re.compile('.+(of)\s|.+,\s|.+(from)\s')

ignore_list = [97, 101]
judgment_count = 1
while judgment_count < 150:
	if judgment_count in ignore_list:
		judgment_count += 1
	else:
		try:
			parse_judgment('FCAFC', judgment_count)
			judgment_count += 1
		except IOError:
			judgment_count += 1