from bs4 import BeautifulSoup
import re, csv, urllib2

def parse_judgment(court_path, count):
	
	#uncomment the following to make soup from URL and not local file

	#url = "http://www.austlii.edu.au/au/cases/cth/FCA/2014/{0}.html".format(judgment_count)
	#req = urllib2.Request(url, headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"})
	#page = urllib2.urlopen(req)
	#soup = BeautifulSoup(page.read(), 'lxml', from_encoding='Latin-1')

	url = '/Users/kelvintran/Dropbox/Python/PowerIndex/{0}/{1}.html'.format(court_path, count)
	soup = BeautifulSoup(open(url), 'lxml', from_encoding='Latin-1')
	
	court = soup.find("h1")	
	
	citation = soup.find(text=re.compile('Citation')).findNext('td').div.get_text().replace('\n', ' ')
	citation = re.split(r'\s\s?(?=\[)', citation)
	
	case_name = citation[0]
	
	MNC = citation[1]
	
	br_tags = soup.find_all('br')
	
	file_num_match = soup.find(text=re.compile('^File number'))
	if file_num_match > -1:
		file_num = str(file_num_match.findNext('td').div.get_text(strip=True))
	else:
		file_num = 'File number error'
	
	judge = soup.find(text=re.compile('^Judge')).findNext('div').get_text().title()
	
	date = soup.find(text=re.compile('^Date of')).findNext('div').get_text()
	
	if soup.find(text=re.compile('^Catchwords:')) == None:
		area_of_law = 'No catchwords'
	else:
		area_of_law = (soup.find(text=re.compile('^Catchwords:')).findNext('b').parent.get_text(strip=True).replace('\n', ' ').replace(u'\u2013', '-'))

	counsel_cat_list_with_newline = soup.findAll(text=re.compile('(^Counsel for\s[A-Za-z]+\s[A-Z][^:]+)|^Counsel for the proposed\s\d+|^Counsel for [A-Za-z\d]+\s[A-Za-z\d]+(\s[A-Za-z\d]+)?(\s[A-Za-z\d]+)?(?=:)'))
	counsel_cat_list = [i.replace('\n', ' ') for i in counsel_cat_list_with_newline]
	counsel_split = [i.findNext('td') for i in counsel_cat_list_with_newline]
	counsel_split = [tag_compile.sub('', str(i)) for i in counsel_split]
	counsel_split = [i.replace('\n', ' ') for i in counsel_split]
	counsel_split = [date_appearance_comp.sub('', str(i)) for i in counsel_split]
	counsel_split = [whitespace_before_after.sub('', str(i)) for i in counsel_split]
	counsel_split = [re.split(r'\b\sand\s\b|\b\swith\s\b|,\s', i) for i in counsel_split]
	
	firm_cat = soup.findAll(text=re.compile('^Solicitor for'))
	firms = [i.findNext('div').renderContents() for i in firm_cat]
	firms = [tag_compile.sub('', i) for i in firms]
	firms = [solicitor_at_firm.sub('', i) for i in firms]

	loss_result = re.compile(ur'(application|proceedings?|appeal).*(dismissed|discontinued)', re.IGNORECASE)
	win_result = re.compile(ur'(application|proceedings?|appeal).*(granted|allowed)', re.IGNORECASE)
	other_win_result = re.compile(ur'pursuant.*(Act|Rules)', re.IGNORECASE)
	orders = soup.find('a', attrs = {'name':'Order_Start_Text'}).parent.get_text(strip=True).replace('\n', ' ')
	if re.search(loss_result, str(orders)):
		result = 'claim failed'
	elif re.search(win_result, str(orders)) or re.search(other_win_result, str(orders)):
		result = 'claim succeeded'
	else:
		result = 'mixed'
		
	print "Citation:", citation
	print "Court:", court.renderContents()

	print "File number:", file_num
	print judge
	print date
	print "counsel_cat_list", counsel_cat_list
	print "counsel_split", counsel_split
	print "firm_cat", firm_cat
	print "firms", firms
	print "Area of law:", area_of_law

	times_to_append = abs(len(firm_cat) - len(counsel_cat_list))
	with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			opened_csv.writerow(['Case name', 'Citation', 'Court', 'File number', 'Judge', 'Date', 'Counsel Role', 'Counsel', 'Firm role', 'Firm'])
	if len(firm_cat) > len(counsel_cat_list):	
		[counsel_cat_list.append('No counsel category') for i in range(times_to_append)]
		[counsel_split.append(['No counsel appearing']) for i in range(times_to_append)]
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firm_cat):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firm_cat[firms_count].encode('UTF-8'), firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1

	elif len(firm_cat) < len(counsel_cat_list):
		[firm_cat.append('No instructing solicitor category') for i in range(times_to_append)]
		[firms.append('No instructing solicitor') for i in range(times_to_append)]
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firm_cat):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firm_cat[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1
	else:
		firms_count = 0
		with open('{0}.csv'.format(court_path), 'a') as f:
			opened_csv = csv.writer(f)
			while firms_count < len(firm_cat):
				counsel_split_count = 0
				while counsel_split_count < len(counsel_split[firms_count]):
					opened_csv.writerow([case_name, MNC, court.renderContents(), file_num, judge, date, counsel_cat_list[firms_count], counsel_split[firms_count][counsel_split_count], firm_cat[firms_count], firms[firms_count], area_of_law])
					counsel_split_count += 1
				firms_count += 1

tag_compile = re.compile('<[^<]+?>')
date_appearance_comp = re.compile('\sappeared\son\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+|\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+')
whitespace_before_after = re.compile(r'^\s|\s$')
solicitor_at_firm = re.compile('.+(of)\s|.+,\s|.+(from)\s')

ignore_list = [108, 177, 178, 631, 756, 794, 845, 849, 850, 856, 858, 861]
judgment_count = 1
while judgment_count < 1500:
	if judgment_count in ignore_list:
		judgment_count += 1
	else:
		try:
			parse_judgment('FCA', judgment_count)
			judgment_count += 1
		except IOError:
			judgment_count += 1

#108 poorly formatted
#177 not there
#178 <br> tags
#631 not there
#756 not there
#794 not there
#794 not there