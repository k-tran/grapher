import urllib2
import os.path

count = 1

jd = 'cth'

def download_judgment(jd, court, count):
	try:
		url = "http://www.austlii.edu.au/au/cases/{0}/{1}/{2}/{3}.html".format(jd, court, '2014', count)
		save_path = '/Users/kelvintran/Dropbox/Python/PowerIndex/' + court + '/'
		file_name = url.split('/')[-1]
		complete_file_name = os.path.join(save_path, file_name)
		req = urllib2.Request(url, headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"})
		u = urllib2.urlopen(req)
		f = open(complete_file_name, 'wb')
		f.write(u.read())
		f.close()

	except urllib2.HTTPError, err:
   		if err.code == 404:
       			print "Page not found!"
		elif err.code == 403:
			print "Access denied!"
		else:
			print "Something happened! Error code", err.code

court = 'HCA'
count = 1
if not urllib2.HTTPError:
	print 'Error'
else:
	while True:
		status = 'Downloading {0}.html'.format(count)
		print status
		download_judgment(jd, court, count)
		count += 1

