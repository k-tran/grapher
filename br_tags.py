from bs4 import BeautifulSoup
import re
import csv

def br_tag_finder(court_path, count):
	url = '/Users/kelvintran/Dropbox/Python/PowerIndex/{0}/{1}.html'.format(court_path, count)	
	soup = BeautifulSoup(open(url), 'lxml', from_encoding='Latin-1')
	citation_with_tag = soup.find(text=re.compile('Citation')).findNext('td').div
	citation_with_tag = str(citation_with_tag)
	citation = tag_compile.sub('', citation_with_tag)
	citation = citation.replace('\n', ' ')
	citation = re.split(r'\s\s?(?=\[)', citation)
	case_name = citation[0]
	MNC = citation[1]
	br_tags = soup.find_all('br')
	print case_name
	print MNC
	count = 0
	with open('br_tags.csv', 'a') as f:
		opened_csv = csv.writer(f)
		while count < len(br_tags):
			opened_csv.writerow([case_name, mnc, br_tags[count]])
			count += 1

#parse_judgment('fca', 3)

tag_compile = re.compile('<[^<]+?>')
date_appearance_comp = re.compile('\sappeared\son\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+|\s[1-9]?[0-9]\s[A-Z][a-z]+\s\d+')
whitespace_before_after = re.compile('^\s|\s$')

judgment_count = 1
while judgment_count < 900:
	if judgment_count == 108 or judgment_count == 177 or judgment_count == 178 or judgment_count == 631 or judgment_count == 756 or judgment_count == 756 or judgment_count == 794 or judgment_count == 845 or judgment_count == 849 or judgment_count == 850 or judgment_count == 856 or judgment_count == 858 or judgment_count == 861:
		judgment_count += 1
	else:
		br_tag_finder('fca', judgment_count)
		judgment_count += 1