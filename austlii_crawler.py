import urllib2
import os.path

count = 1

jd = 'nsw'

def download_judgment(jd, court, count):
	try:
		url = "http://www.austlii.edu.au/au/cases/{0}/{1}/{2}/{3}.html".format(jd, court, '2014', count)
		save_path = '/Users/kelvintran/Dropbox/Python/PowerIndex/' + court + '/'
		file_name = url.split('/')[-1]
		complete_file_name = os.path.join(save_path, file_name)
		req = urllib2.Request(url, headers = {"User-Agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36"})
		u = urllib2.urlopen(req)
		f = open(complete_file_name, 'wb')
		meta = u.info()
		file_size = int(meta.getheaders("Content-Length")[0])
		print "Downloading: %s Bytes: %s" % (complete_file_name, file_size)
		file_size_dl = 0
		block_sz = 8192
		while True:
    			buffer = u.read(block_sz)
    			if not buffer:
    				break
	
    			file_size_dl += len(buffer)
    			f.write(buffer)
    			status = r"%10d  [%3.2f%%]" % (file_size_dl, file_size_dl * 100. / file_size)
    			status = status + chr(8)*(len(status)+1)
    			print status,
		f.close()

	except urllib2.HTTPError, err:
   		if err.code == 404:
       			print "Page not found!"
		elif err.code == 403:
			print "Access denied!"
		else:
			print "Something happened! Error code", err.code

#if jd == 'cth':
#	court_list = ['FCA', 'HCA', 'FCA']
#	for court in court_list:
#		print court
#		count = 1
#		if not urllib2.HTTPError:
#			print 'Now NSW'
#			jd == 'nsw'
#		else:
#			while True:
#				download_judgment(jd, court, count)
#				count += 1

if jd == 'nsw':
	court_list = ['NSWSC', 'NSWCA']
	for court in court_list:
		count = 1
		if not urllib2.HTTPError:
			print 'Now VIC'
			jd == 'vic'
		else:
			while True:
				download_judgment(jd, court, count)
				print type(download_judgment(jd, court, count))
				count += 1
elif jd == 'vic':
	court_list = ['VSC', 'VSCA']
	for court in court_list:
		count = 1
		if Exception:
			print 'Now WA'
			jd == 'wa'
		else:
			while True:
				download_judgment(jd, court, count)
				count += 1
elif jd == 'wa':
	court_list = ['WASC', 'WASCA']
	for court in court_list:
		count = 1
		if Exception:
			print 'Downloading finished'
			break
		else:
			while True:
				download_judgment(jd, court, count)
				count += 1
